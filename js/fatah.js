//objet
/*
    1- creer un objet
    2- ajouter un index:value

*/

// const myCar = new Object();
//     myCar.marque = "peugeot";
//     myCar.year = 1987;
// console.log(myCar);


const myCar = {
    marque : "peugeot",
    year : 1987,
    model : 309
}

myCar.color = "red"; //add un objet
myCar["admin"] = true;

myCar.color += " bright"; //concatiner un objet
myCar.year = "année de fabrication: 1987"; //edit un objet
delete myCar.model //sup un objet
//console.log(("ville" in myCar)); //check propreité dans un objet

for (const keyIndex in myCar) {
  //  console.log(keyIndex)
} //parcourir les index d un objet

for (const keyValue in myCar) {
 //   console.log(myCar[keyValue])
} //parcourir les valeurs d un objet

for (const keyIndexValue in myCar) {
  //  console.log(keyIndexValue + ":" + myCar[keyIndexValue] )
} //parcourir  un objet

const myHome = {
    name : "fatah",
    direBonjour : function () {
        console.log("Bonjour " + this.year);
    } //methode dans objet

}

//console.log(myHome.direBonjour());

// les methodes des objets
//Object.keys
//Object.values
//Object.entries
//Object.assign

const keysIndex = Object.keys(myCar)
const keysValues = Object.values(myCar)
const nestedArray = Object.entries(myCar)
const fusion = Object.assign({}, myHome, myCar)

// console.log(keysIndex);
// console.log(keysValues);
// console.log(nestedArray);
// console.log(fusion);

// construire des objets
/*
-1 creer une fonction avec parametres
-2 stocker les valeur dans les parametres

*/

function Utilisateur(pseudo, ville) {
    this.nom = pseudo;
    this.city = ville;
    this.direBonsoir = function () {
        console.log("Bonsoir " + this.nom + " j'habite à " + this.city);
    } //méthode
}

const user1 = new Utilisateur("fatah", "akbou")
//console.log(user1);
//console.log(user1.direBonsoir());

//Class
class Adresse{
    constructor(numero,code,ville){
        this.numUn = numero;
        this.codeUn = code;
        this.villeUn = ville;
    }
    sayMyAdress = function () {
        console.log("j'habite a l adresse N:" + this.numUn  + ". mon code est le " + this.codeUn);
    }
}

const user2 = new Adresse("9", "0178","paris");
//console.log(user2);

//prototype
Adresse.prototype.sayCity = function () {
    console.log("j'habite à " + this.villeUn);
}
console.log(user2.sayCity());












